import json
import numpy as np
import requests
import psycopg2
from pprint import pprint
import mysql.connector
# from pydantic import BaseModel
# import configparser

# config = configparser.ConfigParser()
# config.read("expert_system/db_config.conf")

# db_config = config['database']

def fun1(_type):
    

    response = requests.get("http://127.0.0.1:8000/get_data")
    data = response.json()
    # print("Value received:", data["table_name"])
    # print("Value received:", data["conn"])
    main_output = {"numbers":[],"strings":[]}
    

    try:
        if _type == "postgres":
            conne = psycopg2.connect(
            host=data["conn"]["host"],
            port=data["conn"]["port"],
            database=data["conn"]["database_name"],
            user=data["conn"]["username"],
            password=data["conn"]["password"]
        )
            cursor1 = conne.cursor()
            table_name = data["table_name"]
            cursor1.execute(f"SELECT column_name FROM information_schema.columns WHERE table_name = %s", (table_name,))
            columns1 = [row[0] for row in cursor1.fetchall()]
        
        elif _type == "mysql": 
            conne = mysql.connector.connect(host=data["conn"]["host"],
            port=data["conn"]["port"],
            database=data["conn"]["database_name"],
            user=data["conn"]["username"],
            password=data["conn"]["password"])
            cursor1 = conne.cursor()
            table_name = data["table_name"]
            cursor1.execute(f"SELECT column_name FROM information_schema.columns WHERE table_name = %s", (table_name,))
            columns1 = [row[0] for row in cursor1.fetchall()]

        
        # for column in columns:
        #     column_name, data_type = column 
        #     main_json(column_name,data_type)

    # main_output = {"numbers":{},"strings":{}}
        
        # kie_input = {"configs":{"version":"v1","mode":"form","inputs":{}},"inputs":[{"numeric_input":{},"string_input":{},"id":"_34E4BEAE-3626-4DE1-ADFC-B6603B8B7ED4"}]}
        # kie_input = {"inputs":[{"numeric_input":{},"string_input":{}}]}

        def nCalc(column_name):
            core_number_dict = {"cnt":None,"sm":None,"mn":None,"mx":None,"unq":None,"cnt_all":None,"q1":None,"q2":None,"q3":None,"outlier_cnt_prsentage":None,"unq_prsentage":None}
            queries  = [
                ##count all things execluding null values 
                {"name": "cnt", "query": f"SELECT COUNT(\"{column_name}\") FROM {table_name};"},
                {"name": "sm", "query":f"SELECT SUM(\"{column_name}\") FROM {table_name};"},
                {"name": "mn", "query":f"SELECT MIN(\"{column_name}\") FROM {table_name};"},
                {"name": "mx", "query":f"SELECT MAX(\"{column_name}\") FROM {table_name};"},
                {"name": "unq", "query":f"SELECT COUNT (DISTINCT(\"{column_name}\")) FROM {table_name};"},
                {"name":"q1", "query":f"SELECT PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY \"{column_name}\") FROM {table_name};",},
                {"name":"q2", "query":f"SELECT PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY \"{column_name}\") FROM {table_name};",},
                {"name":"q3", "query":f"SELECT PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY \"{column_name}\") FROM {table_name};",},
                
                #count all things inclucding null values
                {"name": "cnt_all", "query": f"SELECT COUNT(*) FROM {table_name};"}
            ]

            for query in queries:
                name = query["name"]
                cursor1.execute(query["query"])
                results = cursor1.fetchall()
                core_number_dict.update({name:results[0][0]})      

            query1 = f"SELECT \"{column_name}\" FROM {table_name};"
            cursor1.execute(query1)
            data = cursor1.fetchall()
            
            # Convert fetched data to a numpy array
            data_array = np.array(data)[:, 0]

            # Calculate the median
            median = np.median(data_array)
            
            # Calculate the Median Absolute Deviation (MAD)
            mad = np.median(np.abs(data_array - median))
            
            if mad == 0:
                mad = 1
            # Calculate the Modified Z-Score and count outliers
            threshold = 3.5
            modified_z_scores = (0.6745 * (data_array - median) / mad)
            outliers_count = np.count_nonzero(modified_z_scores > threshold)

            x = (outliers_count*100)/core_number_dict["cnt"]
            core_number_dict["outlier_cnt_prsentage"] = round(x) 
                # if core_number_dict['unq'] == core_number_dict['cnt']:
                #     core_number_dict["cnt_all"] = core_number_dict['unq']
            
            y = (core_number_dict["unq"]*100)/core_number_dict["cnt"]
            core_number_dict["unq_prsentage"] = round(y,4)

            return core_number_dict

        def sCalc(column_name):
            core_string_dict = {"cnt":0,"unq":0 ,"most_freq":"","grp_by":None}
            queries  = [
                {"name": "cnt", "query": f"SELECT COUNT(\"{column_name}\") FROM {table_name} WHERE \"{column_name}\" IS NOT null;"},
                {"name": "unq", "query":f"SELECT COUNT (DISTINCT(\"{column_name}\")) FROM {table_name};"},
                {"name": "most_freq", "query":f"SELECT \"{column_name}\" FROM {table_name} GROUP BY \"{column_name}\" ORDER BY COUNT(*) DESC;"}
            ]

            for query in queries:
                name = query["name"]
                cursor1.execute(query["query"])
                result = cursor1.fetchone()[0]
                core_string_dict.update({name: result})
                
            core_string_dict["grp_by"] = core_string_dict["unq"] <  core_string_dict["cnt"]
            
            return core_string_dict

        def executed():return None

        def main_json(column_name,data_type):
            if data_type == 'integer' or data_type == 'int' or data_type == 'float' or data_type == 'decimal' or data_type == 'double precision':
                makenCalc = nCalc(column_name)
                if makenCalc["q1"]!=makenCalc["q3"] and makenCalc["mn"] >= 0 and makenCalc["cnt"] != makenCalc["unq"]:
                    dict1 = {"attribute_name":column_name,"basic_calculations":makenCalc,"proposed_analysis":executed()}
                    main_output["numbers"].append(dict1)
            elif data_type == 'str' or data_type == 'character varying' or data_type == 'string':
                makesCalc = sCalc(column_name)
                if makesCalc["cnt"] != makesCalc["unq"]:
                    dict2 = {"attribute_name":column_name,"basic_calculations":makesCalc}
                    main_output["strings"].append(dict2)
            return main_output


        for column1 in columns1:
            cursor1.execute(f"SELECT {column1} FROM {table_name} LIMIT %s", (1,))
            sample_values = [row[0] for row in cursor1.fetchall()]
            data_type = type(sample_values[0])
            data_type = data_type.__name__
            column_name1 = column1
            # print(sample_values[0],data_type)
            main_json(column_name1,data_type)

        # json_str1 = json.dumps(main_output, indent=4)
        # with open('./main_output.json', 'w') as file:
        #     file.write(json_str1)

        conne.commit();cursor1.close(); conne.close()
        # pprint(main_output)
        return main_output
                
    except:
        return {"message":"No Connection config provided or wrong confgs","status":False}
        