# Expert System for Predective Decision Automation 

expert is a project for dealing with different types and schemas of databases  .

## Installation & Usage

1. To build the package You firstly should be sure having *wheel* library, install it using `pip install wheel`
2. Build the package *setup.py* by:
```bash
python setup.py sdist bdist_wheel
```
3. Determine your data source configurations by modifying *db_config.conf*
4. In *discover.py* Make sure you defined the wanted table 'table_name' to process it
5. Run web app in your terminal `python app.py`
6. Finally follow the link [http://127.0.0.1:5000/api/final_suggestions]

## Project architecture
![simple flow](./expert_arch.drawio.svg)