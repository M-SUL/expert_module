import psycopg2

def get_attribute_types(connection, table_name, sample_record_count=1):
    cursor = connection.cursor()

    # Get the list of columns in the table
    cursor.execute(f"SELECT column_name FROM information_schema.columns WHERE table_name = %s", (table_name,))
    # print([cursor.fetchall()[0][0]])
    # returns all the names in a list
    columns = [row[0] for row in cursor.fetchall()]
    # print(columns,'\n')
    attribute_types = {}

    for column in columns:
        # Fetch sample records for the column
        cursor.execute(f"SELECT {column} FROM {table_name} LIMIT %s", (sample_record_count,))
        
        sample_values = [row[0] for row in cursor.fetchall()]
        print(sample_values)

        # Identify the Python data type of the attribute
        python_data_type = type(sample_values[0])
        
        
        attribute_types[column] = python_data_type

    cursor.close()
    return attribute_types

# Database connection settings
conn = psycopg2.connect(
    host="localhost",
    port="5432",
    database="restaurant",
    user="test",
    password="test"
)

table_name = "data_pool1"

attribute_types = get_attribute_types(conn, table_name)

# Print the detected attribute types
for column, python_data_type in attribute_types.items():
    print(f"Column: {column} | Python Type: {python_data_type}")

conn.close()