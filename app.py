from typing import Union
from uvicorn import run
from fastapi import FastAPI, HTTPException, Request
from pydantic import BaseModel
import mysql.connector
import requests
import time
from expert import fun2
import uuid
from fastapi.middleware.cors import CORSMiddleware
# import psycopg2
import json

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost:8000",
    "*",
    "http://localhost:8080",
]
app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
class ConnectionConfig(BaseModel):
    host: str
    port: int
    username: str
    password: str
    database_name: str
    type : str
    
# class DataItem(BaseModel):
#     value: bool

connection_config = None
tbl_nm = None
# disconnection_flag = False

@app.post("/connect")
async def db_connection_handler(cnct:ConnectionConfig,tabl_nm:str):

    global connection_config,tbl_nm
    connection_config = cnct
    tbl_nm=tabl_nm
    x = fun2(connection_config.type)
    # print(x)
    if len(x) == 0:
        return {"status": False,"message": "No provided data because of empty data or wrong table name"}
    if "status" in x:
        return x
    else:
        return {"status":True,"message":"New Connection configuration saved and Data fetched successfuly","conn":cnct,"table_name":tbl_nm,"data":x}

@app.get("/get_data")
def get_data():
    global connection_config,tbl_nm


# @app.post("/save_user_config")
# async def save_user_config(request: Request):
#     data = await request.json()
#     print(data)
#     conn = psycopg2.connect(
#         host="localhost",
#         port="5432",
#         database="restaurant",
#         user="test",
#         password="test"
#     )
#     cursor = conn.cursor()
#     cursor.execute("insert into user_config (user_id, config) values(%s, %s)", (data['user_id'], json.dumps(data['config'])))
#     {
#         "user_id":"",
#         "config":{
#             'cnt': 0,
#             'unq': 5,
#             'var': 0.0015,
#             'var_mn_1': 0,
#             'var_mn_2': 1,
#             'sum': 1,
#             'avg':  10,
#         }
#     }
    
#     return {"message": "done deal"}


if __name__ == "__main__":
    run("app:app", reload=True)