from experta import *
import json
from discover import fun1
import requests
from pprint import pprint
from config import conf


def fun2(_type):

    data = fun1(_type)
    if "status" in data: 
        return data        
    # print(data)
    # with open('./main_output.json', "r") as json_file_r:
    #     data = json.load(json_file_r)
    
    def mapping(data):
        datacnt =0
        final_suggestions = []
        output_json = []
        dict = {"fact":None,"dimension":None,"applied_fun":None}
        for numeric_entry in data["numbers"]:
            for string_entry in data["strings"]:
                dict.update({"fact":numeric_entry["attribute_name"],"applied_fun":numeric_entry["proposed_analysis"]})    
                if string_entry["basic_calculations"]["grp_by"] == True:
                    dict.update({"dimension":string_entry["attribute_name"]})
                    final_suggestions.append(dict)    
                dict = {}
       
        for entry in final_suggestions:
            applied_funs = entry["applied_fun"]
            for applied_fun in applied_funs:
                new_entry = entry.copy()
                new_entry["applied_fun"] = applied_fun
                output_json.append(new_entry)
                datacnt=datacnt+1
        print(datacnt)

        return output_json

    class Input(Fact):
        pass

    class RuleEngine(KnowledgeEngine):
        
        @Rule(Input(
            numeric_input_cnt_eq_cntall=P(conf['cnt'])
        ))
        def rule_cnt(self):
            # global rule_output
            rule_output.extend(["cnt"])
            
        @Rule(Input(
            numeric_input_unq_prsentage=P(conf['unq'])
        ))
        def rule_unq(self):
            # global rule_output
            rule_output.extend(["unq"])
            
        @Rule(Input(
            numeric_input_max_sum_data=P(conf['var'])
            ))
        def rule_var(self):
            # global rule_output
            rule_output.extend(["var"])
            
            
        @Rule(Input(
            numeric_input_min_sum_data=P(conf['var']),
            numeric_input_mn=P(conf['var_mn'])
            ))
        def rule_var2(self):
            # global rule_output
            rule_output.extend(["var"])
                    
        @Rule(Input(
            numeric_input_cnt_mx=P(conf['sum'])
        ))
        def rule_sum(self):
            # global rule_output
            rule_output.extend(["sum"])

        # @Rule(Input(
        #     numeric_input_unq_prsentage=P(lambda x: x >0.06)
        # ))
        # def rule_q2(self):
        #     # global rule_output
        #     rule_output.extend(["q2"])

        # @Rule(Input(
        #     numeric_input_unq_prsentage=P(lambda x: x >1)
        # ))
        # def rule_q1(self):
        #     # global rule_output
        #     rule_output.extend(["q1"])

        # @Rule(Input(
        #     numeric_input_unq_prsentage=P(lambda x: x >1)
        # ))
        # def rule_q3(self):
        #     # global rule_output
        #     rule_output.extend(["q3"])
        
        @Rule(Input(
            numeric_input_outlier_cnt_prsentage=P(conf['avg'])
            ))
        def rule_avg(self):
            # global rule_output
            rule_output.extend(["avg"])
            
            
    rule_output = []


    engine = RuleEngine()
    # pprint(data['numbers'])
    for numeric_entry in data['numbers']:
        numeric_name = numeric_entry['attribute_name']
        numeric_input = numeric_entry['basic_calculations']
        engine.reset()  # Reset the engine before asserting facts
        engine.declare(Input(
                            numeric_input_cnt=round(numeric_input['cnt']),
                            numeric_input_max_sum_data=round(numeric_input['mx']/numeric_input['sm']),
                            numeric_input_min_sum_data=round(numeric_input['mn']/numeric_input['sm']),
                            numeric_input_mn=round(numeric_input['mn']),
                            # numeric_input_mx=numeric_input['mx'],
                            # numeric_input_sm=numeric_input['sm'],
                            # numeric_input_unq=numeric_input['unq'],
                            numeric_input_unq_prsentage=round(numeric_input['unq_prsentage']),
                            # numeric_input_cnt_all=numeric_input['cnt_all'],
                            numeric_input_cnt_eq_cntall=round(numeric_input['cnt_all'] - numeric_input['cnt']),
                            # numeric_input_cnt_unq= numeric_input['unq']/numeric_input['cnt'],
                            numeric_input_cnt_mx=round((numeric_input['sm']-numeric_input['mx'])/numeric_input['mx']),
                            #count of outlier values(has a Z-Score > 3.5)
                            numeric_input_outlier_cnt_prsentage = round(numeric_input['outlier_cnt_prsentage'])
                        ))
        engine.run()
        numeric_entry['proposed_analysis'] = list(set(rule_output))
        # main_output = data
        # with open('final_output.json', 'w') as json_file_w:
        #     json.dump(data, json_file_w, indent=4)
        rule_output = []
    # pprint(data)
    
    return mapping(data)