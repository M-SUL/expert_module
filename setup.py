from setuptools import setup

setup(
    name="expert_system",
    version="0.1",
    description="An expert system module for predective decision automation",
    packages=["expert_system"],
    install_requires=[
        "experta",
        "frozendict==2.3.4",
        "psycopg2",
        "configparser",
        "fastapi",
        "requests",
        "pydantic",
        "uvicorn",
        "numpy",
        "MySQL-python",
        "mysql-connector-python"
        # "flask",
        # "markupsafe==1.1.1",
        # "itsdangerous==1.1.0",
        
    ],
)